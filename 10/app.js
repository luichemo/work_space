function p1() {
    var p1 = document.getElementById("p1");
    const num = 1.5;
    p1.innerHTML = Math.ceil(num);
}
p1();

function p2() {
    var p2 = document.getElementById("p2");
    const num = 1.5;
    p2.innerHTML = Math.floor(num);
}
p2();

function p3() {
    var p3 = document.getElementById("p3");
    const num = 1.5;
    p3.innerHTML = Math.round(num);
}
p3();

function p4(x, y) {
    var p4 = document.getElementById("p4");
    if (y == true) {
        p4.innerHTML = document.write(Math.ceil(x));
    } else {
        p4.innerHTML = Math.floor(x);
    }
}

p4(12.4, true);

// function p5() {
//     var randomNum = Math.random() * 1;
//     document.getElementById("p5").innerHTML = randomNum;
// }
// p5();

// function p6(min, max) {
//     return Math.floor(
//         Math.random() * (max - min) + min
//     )
// }

// document.write(
//         p6(5, 20)